# music-split-merge
This project contains some scripts that are useful to manipulate old school mp3 songs. Those scripts are tailored to my needs and currently not usable by other persons without changing hard coded values.

In the following part, you can find a small description of the purpose of those scripts:

## main.py
Can be used to fix songs, where the end of the previous song is actually part of the beginning of the following song.

My use case: I record songs from an online radio stream automatically. However, during this process, the created mp3s are not split correctly. Thus, I need to manually extract the last part of a song and add it to the beginning of the next recorded song. This script simplifies this process by using shortcuts via the command line and automatically adjusts the songs accordingly.

This scipt can be used if changing the hard-coded paths.

## music-retag.py
Can be used to retag song files with other album or artists names...

Not usable by other persons right now.

## music-rsync.py
Can be used to rsync music files with android via the adb interface.

This script is also usable when changing the hard-coded paths.